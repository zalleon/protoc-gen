# Protocol puffers tools

## Tools
* protoc
* grpc_php_plugin
* grpc_csharp_plugin
* grpc_objective_c_plugin
* protoc_gen_go
* protoc_gen_grpc_java
* protoc-gen-grpc-gateway
* protoc-gen-swagger
* prototool

## Common files:

* protocolbuffers/protobuf/src/google
* googleapis/api-common-protos/google

 - Binaries are located at /usr/local/bin/.
 - Includes are located at /usr/local/include/.

## Usage
```bash
# specify {VERSION}
alias dproto="docker run -v $(pwd):/defs -it registry.gitlab.com/zalleon/protoc-gen:{VERSION}"

dproto protoc \
    -I=/usr/local/include \
    --proto_path=. \
    --go_out=plugins=grpc:. \
    --plugin=protoc-gen-go=/usr/local/bin/protoc-gen-go \
    service.proto
dproto protoc \
    -I=/usr/local/include \
    --proto_path=. \
    --php_out=. \
    --grpc_out=. \
    --plugin=protoc-gen-grpc=/usr/local/bin/grpc_php_plugin \
    service.proto

echo "lint:\n  group: uber2" > ./prototool.yaml
dproto prototool \
    lint \
    --protoc-bin-path=/usr/local/bin/protoc \
    --protoc-wkt-path=/usr/local/include \
    service.proto
```
