NAMESPACE?=zalleon
APP?=protoc-gen
RELEASE?=latest

PROTOBUF_VER?=3.7.1
GRPC_VER?=1.20.0
PROTOC_GEN_GO_VER?=1.3.1
PROTOC_GRPC_GATEWAY_VER?=1.8.5
GOOGLE_APIS_VER?=0.1.0
PROTOTOOL_VER?=1.6.0

GIT_VCS_HOST?=gitlab.com
DOCKER_REGISTRY_HOST?=registry.gitlab.com

GIT_REPO?=${GIT_VCS_HOST}/${NAMESPACE}/${APP}
IMAGE_REPO?=${DOCKER_REGISTRY_HOST}/${NAMESPACE}/${APP}

.PHONY: image
image:
	@echo "+ $@"
	docker build \
	--build-arg PROTOBUF_VER=${PROTOBUF_VER} \
	--build-arg GRPC_VER=${GRPC_VER} \
	--build-arg PROTOC_GEN_GO_VER=${PROTOC_GEN_GO_VER} \
	--build-arg PROTOC_GRPC_GATEWAY_VER=${PROTOC_GRPC_GATEWAY_VER} \
	--build-arg GOOGLE_APIS_VER=${GOOGLE_APIS_VER} \
	--build-arg PROTOTOOL_VER=${PROTOTOOL_VER} \
	--pull -t ${IMAGE_REPO}:${RELEASE} .

.PHONY: push
push: image
	@echo "+ $@"
	docker push ${IMAGE_REPO}:${RELEASE}
