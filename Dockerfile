# == PRE_BUILD ==
FROM golang:1.12 AS build

WORKDIR /tmp

RUN apt-get update && apt-get install -y unzip make cmake autoconf automake libtool

# protobuf
ARG PROTOBUF_VER
RUN cd /tmp && git clone -b v${PROTOBUF_VER} https://github.com/protocolbuffers/protobuf
RUN cd /tmp/protobuf && ./autogen.sh && ./configure --disable-shared && make && make install

# grpc
ARG GRPC_VER
RUN cd /tmp && git clone -b v${GRPC_VER} https://github.com/grpc/grpc
RUN cd /tmp/grpc && git submodule update --init

# grpc_php_plugin
RUN cd /tmp/grpc && make grpc_php_plugin

# protoc_gen_go
ARG PROTOC_GEN_GO_VER
RUN GO111MODULE=on go get -u github.com/golang/protobuf/protoc-gen-go@v${PROTOC_GEN_GO_VER}

# grpc gateway
ARG PROTOC_GRPC_GATEWAY_VER
RUN GO111MODULE=on go get -u github.com/grpc-ecosystem/grpc-gateway/protoc-gen-grpc-gateway@v${PROTOC_GRPC_GATEWAY_VER}
RUN GO111MODULE=on go get -u github.com/grpc-ecosystem/grpc-gateway/protoc-gen-swagger@v${PROTOC_GRPC_GATEWAY_VER}
RUN ln -s \
    /go/pkg/mod/github.com/grpc-ecosystem/grpc-gateway@v${PROTOC_GRPC_GATEWAY_VER}/protoc-gen-swagger/ \
    /usr/local/include/protoc-gen-swagger

# google apis
ARG GOOGLE_APIS_VER
RUN cd /tmp && git clone https://github.com/googleapis/api-common-protos
RUN cd /tmp/api-common-protos && git checkout v${GOOGLE_APIS_VER}

# prototool
ARG PROTOTOOL_VER
RUN curl -sSL https://github.com/uber/prototool/releases/download/v${PROTOTOOL_VER}/prototool-$(uname -s)-$(uname -m) \
    -o /usr/local/bin/prototool && chmod +x /usr/local/bin/prototool

# == BUILD ==
FROM alpine:3.8

RUN set -ex && apk --update --no-cache add bash libstdc++ libc6-compat ca-certificates
RUN mkdir /defs
WORKDIR /defs

COPY --from=build /usr/local/bin/ /usr/local/bin/
COPY --from=build /go/bin/ /usr/local/bin/
COPY --from=build /tmp/grpc/bins/opt/ /usr/local/bin/

COPY --from=build /usr/local/include/protoc-gen-swagger/options/ /usr/local/include/protoc-gen-swagger/options/
COPY --from=build /tmp/api-common-protos/google/ /usr/local/include/google/
COPY --from=build /tmp/protobuf/src/google/ /usr/local/include/google/

# clear includes
RUN find /usr/local/include/google -type f ! -iname "*.proto" -delete
RUN find /usr/local/include/google -type f -iname "*_test.proto" -delete
RUN for dir in `find /usr/local/include/google -type d | sort -ur`; do \
        if [ $(find $dir -type f | wc -l ) -eq 0 ]; then \
            rm -rf $dir; \
        fi \
    done
